sudo addgroup groupe_a
sudo adduser --disabled-password --gecos '' --ingroup groupe_a lambda_a
sudo passwd -d lambda_a

sudo addgroup groupe_b
sudo adduser --disabled-password --gecos '' --ingroup groupe_b lambda_b
sudo passwd -d lambda_b

sudo adduser --disabled-password --gecos '' -- admin
sudo passwd -d admin
sudo usermod -aG sudo admin
sudo usermod -a -G groupe_a admin
sudo usermod -a -G groupe_b admin

sudo mkdir dir_a
sudo chow root:groupe_a dir_a
sudo chgrp groupe_a dir_a
sudo chmod o-rwx dir_a
sudo chmod g+ws dir_a
sudo chmod +t dir_a

sudo mkdir dir_b
sudo chow root:groupe_b dir_b
sudo chgrp groupe_b dir_b
sudo chmod o-rwx dir_b
sudo chmod g+ws dir_b
sudo chmod +t dir_b

sudo mkdir dir_c
sudo chmod o-w dir_c
sudo chmod o-x dir_c
sudo chmod g+s dir_c
sudo chmod +t dir_c

sudo runuser -l admin -c 'sudo touch ../ubuntu/dir_c/file_c_admin'
sudo runuser -l admin -c 'sudo touch ../ubuntu/dir_a/file_a_admin'
sudo runuser -l lambda_a -c 'touch ../ubuntu/dir_a/file_a'
sudo runuser -l admin -c 'sudo touch ../ubuntu/dir_b/file_b_admin'
sudo runuser -l lambda_b -c 'touch ../ubuntu/dir_b/file_b'