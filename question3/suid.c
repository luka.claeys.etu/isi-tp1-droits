#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
    FILE *f;
    
    if(argc<2){
       printf("Missing argument\n");
       exit(EXIT_FAILURE);
    }

    printf("EUID : %d\n",geteuid());
    printf("EGID : %d\n",getegid());
    printf("RUID : %d\n",getuid());
    printf("RGID : %d\n",getgid());

    f = fopen(argv[1],"r");
    if(f==NULL){
       printf("Cannot open file");
       exit(EXIT_FAILURE);
    }
    else{
       printf("File opens correctly");
       fclose(f);
    }
    return 0;
}
