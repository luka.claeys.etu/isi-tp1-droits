# Rendu "Les droits d’accès dans les systèmes UNIX"

## Binome

- Nom, Prénom, email: Claeys Luka luka.claeys.etu@univ-lille.fr

## Question 1

Il ne peut pas écrire car l'EUID correspond à l'UID de toto donc, c'est le premier triplet de permissions qui est utilisé et celui-ci n'autorise que la lecture.

## Question 2

Pour un dossier,x correspond à l'autorisation de l'ouvrir. 
Si toto essaye d'ouvrir mydir, il recevra une erreur permission dinied puisqu'il n'es pas le propriétaire du fichier mais que l'un de ses groupes correspond à ceux qui ont des droits sur le dossier. De ce fait, les permissions sont celles du 2ème triplet qui n'a pas les droit d'execution.
Le ls -al mydir n'affiche aucune informations à part le nom des dossiers et fichiers présents dans mydir. Cela est également du à l'absence de 'autorisation d'execution.

## Question 3

Sans le flag, les valeurs sont EUID: 1001, EGID: 1001, RUID: 1001 et RGID: 1001. Le processus ne peut lire le fichier.

Avec le flag, les valeurs sont les même sauf pour le EUID qui vaut 1000. Le processus peut lire le fichier.

## Question 4

Les deux ids ont la même valeur: 1001.

## Question 5

chfn sert à modifier les informations liées à l'utilisateur courrant.
-rwsr-xr-x 1 root root 85064 May 28 2020 /usr/bin/chfn

## Question 6

Les mots de passe sont stockés dans un autre fichier, /ect/shadow, que seul l'administrateur peut accéder. De plus, les mots de passe sont cryptés. Ceci a pour but de sécuriser l'accès aux mots de passe puisqu'il y a moins de droit d'accès et aussi parce que la fonction chfn peut modifier le fichier passwd.

## Question 7

Mettre les scripts bash dans le repertoire *question7*.

## Question 8

Le programme et les scripts dans le repertoire *question8*.

## Question 9

Le programme et les scripts dans le repertoire *question9*.

## Question 10

Les programmes *groupe_server* et *groupe_client* dans le repertoire
*question10* ainsi que les tests. 








