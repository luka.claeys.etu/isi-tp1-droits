import os,pwd

def delete(file):
    if(check_group(file)):
        check_pass(file)
    else:
        print("Vous n'avez pes les droits nécessaires")
    return

def check_group(file):
    info = os.stat(file)
    gid = pwd.getpwnam(os.getlogin()).pw_gid
    return info.st_gid == gid
    

def check_pass(file):
    uid = os.getuid()
    passwd = input("Entrez votre mot de passe : ")
    f = open('../admin/passwd',"r")
    line = f.readline()
    
    while line:
        l = line.split()
        if uid==int(l[0]):
            if l[1]==passwd:
                os.remove(file)
                print("Fichier supprimer")
            else:
                print("Mauvais mot de passe")
            return
        else:
            line = f.readline()
    print("Utilisateur non reconnu")
    return
    
    
    
if __name__ == '__main__':
    args = sys.argv[1:]
    delete(args[0])